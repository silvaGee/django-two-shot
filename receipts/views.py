from django.shortcuts import render, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


# Create your views here.
@login_required
#to make this work for Feature8, we have to add login required
#and we do that with decorator
def home(request):
    receipt_all_objects = Receipt.objects.filter(purchaser=request.user)
    #we also need to change Receipt.objects.all to filter per the instructions of Feature 8
    context = {
        "receipt_list_view": receipt_all_objects
    }
    #this code gets all the instances of the Receipt model
    #and puts them in the context for the template
    return render(request, "receipts/list.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        create = ReceiptForm(request.POST)
        if create.is_valid():
            receipt = create.save (commit=False)
            #we put False here because the purchaser attribute uses the foreignkey
            #and at this step in the code, the save won't be associated with the user
            #and in line 25 we say the purchaser is the user, so then the save now becomes associated
            #with the user in the next line receipt.save
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        create = ReceiptForm()
    context = {
        "form": create,
    }
    return render(request, "receipts/create.html", context)

@login_required
def category_list(request):
    receipt_category_objects=ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "expense_categories":receipt_category_objects
    }
    return render(request, "receipts/categories.html", context)

@login_required
def create_category(request):
    if request.method == "POST":
        create = ExpenseCategoryForm(request.POST)
        if create.is_valid():
            create_expense = create.save (commit=False)
            #we put False here because the purchaser attribute uses the foreignkey
            #and at this step in the code, the save won't be associated with the user
            #and in line 25 we say the purchaser is the user, so then the save now becomes associated
            #with the user in the next line receipt.save
            create_expense.owner = request.user
            create_expense.save()
            return redirect("category_list")
    else:
        create = ExpenseCategoryForm()
    context = {
        "form": create,
    }
    return render(request, "receipts/create_categories.html", context)


@login_required
def account_list(request):
    receipts_account_objects=Account.objects.filter(owner=request.user)
    context = {
        "account_categories":receipts_account_objects
    }
    return render(request, "receipts/accounts.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        create = AccountForm(request.POST)
        if create.is_valid():
            create_post = create.save (commit=False)
            create_post.owner = request.user
            create_post.save()
            return redirect("account_list")
    else:
        create = AccountForm()
    context = {
        "form":create,
    }
    return render(request, "receipts/create_account.html", context)
