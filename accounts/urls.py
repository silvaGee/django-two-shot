from django.urls import path
from accounts.views import userlogin, userlogout, signup

urlpatterns = [
    path("login/", userlogin, name="login"),
    path("logout/", userlogout, name="logout"),
    path("signup/", signup, name="signup"),

]
